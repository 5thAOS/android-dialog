package com.example.rany.dialogdemo;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CustomDialog extends DialogFragment{

    EditText username, password;
    Button btnLogin;
    String name;
    String psw;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_layout, null);
         username = view.findViewById(R.id.edtLoginName);
         password = view.findViewById(R.id.edtLogingPsw);
         btnLogin = view.findViewById(R.id.btnLogin);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = username.getText().toString();
                psw = password.getText().toString();
                Toast.makeText(getActivity(),  name +" ,"+ psw, Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
    }
}

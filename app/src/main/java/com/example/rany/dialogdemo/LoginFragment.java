package com.example.rany.dialogdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginFragment extends DialogFragment {

    EditText edtUsername, edtPassword;
    Button btnLogin;
    String user, psw;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle(R.string.welcome)
                .setCancelable(true)
                .setView(layoutInflater.inflate(R.layout.login_layout ,null))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edtUsername = getDialog().findViewById(R.id.edtLoginName);
                        edtPassword = getDialog().findViewById(R.id.edtLogingPsw);

                        user = edtUsername.getText().toString();
                        psw = edtPassword.getText().toString();
                        Toast.makeText(getActivity().getApplicationContext(), user + " "+ psw, Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        return builder.create();


    }
}
package com.example.rany.dialogdemo;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CustomeDialogFragemnt extends DialogFragment{

    Button btnLogin;
    EditText edtName, edtPsw;
    String name, psw;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_layout, container);
        btnLogin = view.findViewById(R.id.btnLogin);
        edtName = view.findViewById(R.id.edtLoginName);
        edtPsw = view.findViewById(R.id.edtLogingPsw);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = edtName.getText().toString();
                psw = edtPsw.getText().toString();

                Toast.makeText(getActivity().getApplicationContext(), name +" "+ psw, Toast.LENGTH_SHORT).show();

            }
        });


    }
}

package com.example.rany.dialogdemo;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class MainActivity extends AppCompatActivity {

    Button simple_dialog, single_choice, radio_dialog, multi_dialog, fragment_dialog, custom_dialog, sweet_alert;
    String[] model;
    int position = -1;
    String[] brand  = {"AB", "CD", "DE", "GF"};
    String[] courses ;

    List<String> course_sub = new ArrayList<>();
    boolean[] course_checked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // find reference between xml and java
        initView();

        course_checked = new boolean[courses.length];

        simple_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSimpleDialog();
            }
        });

        single_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSingleChoiceDialog();
            }
        });

        radio_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRadioDialog();
            }
        });

        multi_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMultiChoiceDialog();
            }
        });

        sweet_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(MainActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Are you sure?")
                        .setContentText("Won't be able to recover this file!")
                        .setConfirmText("Yes,delete it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });

        fragment_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentDialog fragmentDialog = new FragmentDialog();
                fragmentDialog.show(getFragmentManager(), "Greeting");
            }
        });

        custom_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
//                LoginFragment custom = new LoginFragment();
//                custom.show(fragmentManager, "Login");

                CustomDialog customDialog = new CustomDialog();
                customDialog.show(fragmentManager, "Custom");
            }
        });


    }


    void initView(){
        simple_dialog = findViewById(R.id.btnSimple);
        model = getResources().getStringArray(R.array.model);
        single_choice = findViewById(R.id.btnSingleChoice);
        radio_dialog = findViewById(R.id.btnRadioDialog);
        multi_dialog = findViewById(R.id.btnMultiDialog);
        courses = getResources().getStringArray(R.array.course);
        fragment_dialog = findViewById(R.id.btnFragmentDia);
        custom_dialog = findViewById(R.id.btnCustomDia);
        sweet_alert = findViewById(R.id.btnSweetalert);
    }


    void showMultiChoiceDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.course)
                .setCancelable(false)
                .setMultiChoiceItems(courses, course_checked, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i, boolean isChecked) {
                        if(isChecked){
                            course_sub.add(courses[i]);
                        }
                        else if(course_sub.contains(courses[i])){
                            course_sub.remove(courses[i]);
                        }
                       // Toast.makeText(MainActivity.this, course_sub+"", Toast.LENGTH_LONG).show();
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String result = "";
                        for(int i = 0; i < course_sub.size(); i++){
                            result += course_sub.get(i);
                            if (i < (course_sub.size()-1)){
                                result +=" , ";
                            }
                        }
                        Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    void showRadioDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.model)
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setSingleChoiceItems(brand, position, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, brand[which], Toast.LENGTH_SHORT).show();
                        position = which;
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
//                        String result = brand[i];
                        Toast.makeText(MainActivity.this, brand[position], Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .show();
    }

    void showSingleChoiceDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.model)
                .setCancelable(false)
                .setItems(model, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, model[which], Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    void showSimpleDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.welcome)
                .setMessage("Welcome to Cambodia Kingdom of Wonder")
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Accept", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Deny", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .show();
//        AlertDialog alertDialog = builder.create();
//        alertDialog.show();
    }
}
